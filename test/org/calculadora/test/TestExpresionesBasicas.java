package org.calculadora.test;

import static org.junit.Assert.assertEquals;

import org.calculadora.core.Expresion;
import org.calculadora.excpt.OperacionInvalida;
import org.junit.Test;

public class TestExpresionesBasicas {

	@Test
	public void testSuma() {
		assertEquals(5D, new Expresion("sumar 2 y 3").resolver().doubleValue(), 0);
	}
	
	@Test
	public void testResta() {
		assertEquals(3D, new Expresion("restar 5 y 2").resolver().doubleValue(), 0);
	}
	
	@Test
	public void testMultiplicar() {
		assertEquals(9D, new Expresion("multiplicar 3 y 3").resolver().doubleValue(), 0);
	}
	
	@Test
	public void testDividir() {
		assertEquals(3D, new Expresion("dividir 6 y 2").resolver().doubleValue(), 0);
	}
	
	@Test(expected=OperacionInvalida.class)
	public void testDividirEntre0() {
		new Expresion("dividir 6 y 0").resolver();
	}
	
	@Test
	public void testPotencia() {
		assertEquals(4D, new Expresion("potencia 2 y 2").resolver().doubleValue(), 0);
	}

}
