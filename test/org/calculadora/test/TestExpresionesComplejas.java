package org.calculadora.test;

import static org.junit.Assert.*;

import org.calculadora.core.Expresion;
import org.junit.Test;

public class TestExpresionesComplejas {

	@Test
	public void testDosExpresiones() {
		assertEquals(8D, new Expresion("sumar 2 y multiplicar 2 y 3").resolver().doubleValue(), 0);
	}

	@Test
	public void testExpresionesAnidadas() {
		assertEquals(17D, new Expresion("sumar restar 4 y 5 y multiplicar 2 y sumar 4 y 5").resolver().doubleValue(), 0);
	}
}
