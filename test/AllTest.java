import org.calculadora.test.TestExpresionesBasicas;
import org.calculadora.test.TestExpresionesComplejas;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	TestExpresionesBasicas.class,
	TestExpresionesComplejas.class
})
public class AllTest {

} 
