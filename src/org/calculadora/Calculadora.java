package org.calculadora;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.calculadora.core.Expresion;
import org.calculadora.excpt.ExpresionInvalida;
import org.calculadora.excpt.OperacionInvalida;


/**
 http://www.solveet.com/exercises/calculadora-en-string-java/282
 
 Instrucciones Generales: El estudiante debe de implementar una soluci�n en JAVA 
para interpretar expresiones. El objetivo del programa consiste en solicitarle al 
usuario que digite una expresi�n y que el programa interprete esta expresi�n y 
muestre el resultado correspondiente:
Ejemplo
Digite expresi�n:
>> sumar 2 y 3
resultado: 5

La sintaxis debe una expresi�n es la forma en la cual esta es definida, en este 
caso puede ser un literal(un numero) o una expresion binaria
Operacion Expresion1 y Expresion1

Reglas
�	Se debe suportar las operaciones: sumar, restar, multiplicar, dividir, potencia
�	Se debe de respetar la sintaxis expuesta en este documento
�	Se debe indicar si existe un error de sintaxis
�	Mostrar un error para la divisi�n entre 0
�	Se debe soportar expresiones anidadas

Expresiones Anidadas
El soporte en expresiones anidadas consiste en evaluar una expresi�n y el resultado de esta es usado en otra expresi�n, no existe un un limite entre los niveles de anidaci�n soportados
�	sumar 2 y multiplicar 2 y 3 => sumar 2 y 6 => 8
�	sumar restar 4 y 5 y multiplicar 2 y sumar 4 y 5 => sumar restar 4 y 5 y multiplicar 2 y 9 => sumar -1 y 18 =>17
�	
Notas Adicionales
�	Aplicar POO
�	Deben existir diferentes clases(ejemplo parseador, expresion, etc.)
�	Modificadores de acceso 
�	Se debe usar paquetes
�	Uso de convenciones para programar en java
 * */
public class Calculadora {

	public static void main(String[] args) throws IOException {
		while(true){
			try {
				System.out.println("Introduzca una expresi�n a evaluar: ");
				BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
				String expresion = bufferRead.readLine();
				Double resultado = new Expresion(expresion).resolver();
				System.out.println("Resultado: " + resultado);
			} catch (OperacionInvalida oi) {
				System.err.println("Operacion inv�lida: " + oi.getMessage());
			} catch (ExpresionInvalida ei) {
				System.err.println("Expresion inv�lida: " + ei.getMessage());
			}
		}
	}

}
