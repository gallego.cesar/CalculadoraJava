package org.calculadora.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.calculadora.core.operaciones.Division;
import org.calculadora.core.operaciones.Multiplicacion;
import org.calculadora.core.operaciones.Potencia;
import org.calculadora.core.operaciones.Resta;
import org.calculadora.core.operaciones.Suma;
import org.calculadora.excpt.ExpresionInvalida;

/**
 * Para la resolucion usaremos un modelo de sustitución. Tal y como se plantea:
 * 
 * sumar 2 y multiplicar 2 y 3 => sumar 2 y 6 => 8
 * 
 * */
public class Expresion {
	// cuidado con la preferencia de los operadores
	private Operacion[] operaciones = new Operacion[]{
			new Potencia(),
			new Division(),
			new Multiplicacion(),
			new Resta(),
			new Suma()
	};
	private static Pattern completa = Pattern.compile("^([-0-9.]+)\\s*$");
			
	private String expresion;
	
	public Expresion(String expresion) {
		this.expresion = expresion;
	}

	public Double resolver(){
		boolean valida;
		while(!completa.matcher(expresion).find()){
			valida = false;
			for(Operacion op : operaciones){
				if(op.aplica(expresion)){
					valida=true;
					expresion = op.sustituye(expresion);
				}
			}
			if(!valida){
				throw new ExpresionInvalida("operación inválida "+expresion);
			}
		}
		Matcher m = completa.matcher(expresion);
		if(m.find())
			return Double.valueOf( m.group(1) );
		return 0D;
	}
}
