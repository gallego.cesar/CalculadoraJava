package org.calculadora.core;

public interface Operacion {
	public boolean aplica(String expresion);
	public String sustituye(String expresion);
}
