package org.calculadora.core.operaciones;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.calculadora.core.Operacion;
import org.calculadora.excpt.OperacionInvalida;

public class Division implements Operacion {
	private static final String DIVISION = "dividir\\s+([-0-9.]+)\\s+y\\s+([-0-9.]+)\\s*";

	@Override
	public boolean aplica(String expresion) {
		Pattern patron = Pattern.compile(DIVISION);
		return patron.matcher(expresion).find();
	}

	@Override
	public String sustituye(String expresion) {
		Pattern patron = Pattern.compile(DIVISION);
		Matcher m = patron.matcher(expresion);
		m.find();
		Double a,b;
		a=Double.valueOf(m.group(1));
		b=Double.valueOf(m.group(2));
		if(b.equals(0D))
			throw new OperacionInvalida("divisi�n entre 0");
		String resultado = new Double(a/b).toString();
		return expresion.replaceFirst(DIVISION, resultado+" ");
	}


}
