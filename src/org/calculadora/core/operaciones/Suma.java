package org.calculadora.core.operaciones;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.calculadora.core.Operacion;

public class Suma implements Operacion {
	private static final String SUMA = "sumar\\s+([-0-9.]+)\\s+y\\s+([-0-9.]+)\\s*";

	@Override
	public boolean aplica(String expresion) {
		Pattern patron = Pattern.compile(SUMA);
		return patron.matcher(expresion).find();
	}

	@Override
	public String sustituye(String expresion) {
		Pattern patron = Pattern.compile(SUMA);
		Matcher m = patron.matcher(expresion);
		m.find();
		Double a,b;
		a=Double.valueOf(m.group(1));
		b=Double.valueOf(m.group(2));
		String resultado = new Double(a+b).toString();
		return expresion.replaceFirst(SUMA, resultado+" ");
	}


}
