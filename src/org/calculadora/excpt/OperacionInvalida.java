package org.calculadora.excpt;

public class OperacionInvalida extends RuntimeException {

	private static final long serialVersionUID = -157734182865737298L;

	public OperacionInvalida(String message) {
		super(message);
	}
}
