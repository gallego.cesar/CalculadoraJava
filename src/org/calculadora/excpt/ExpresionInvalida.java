package org.calculadora.excpt;

public class ExpresionInvalida extends RuntimeException {

	private static final long serialVersionUID = 7711288223496192548L;

	public ExpresionInvalida(String message) {
		super(message);
	}
}
